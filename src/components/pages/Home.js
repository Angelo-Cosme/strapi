import React from 'react';

import Slider from '../includes/banner';
import Products from '../includes/products';
import Grow from '../includes/grow';
import Order from '../includes/order';


class Home extends React.Component {
  render() {
    return(
      <div>
        <Slider/>
        <Products/>
        <Grow/>
        <Order/>
      </div>
    ); 
  }
}

export default Home;
