import React from "react";
import { Link } from "react-router-dom";
import Prices from "../images/feature.png";


function Order () {
  return (
    <div id="order" className="py-5">
      <div className="container">
      <div className="row">
          <div className="col-lg-12 text-center">
            <h2>Are You Ready?</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6">
          <img src={Prices} class="img-fluid mt-4" alt="Service-Cloud"/>
          </div>

          <div className="col-lg-6">
          <div class="pricing-plan text-center">
            <div class="pricing-title">
              <span>By Month</span>
            </div>
            <div class="price">
              <span>$</span> 50
            </div>
              <Link to="/Contact" class="price-btn btn" href="#">Contact Us</Link>
          </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Order;
