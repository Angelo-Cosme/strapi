
import React from "react";
import Server from "../images/Server.png";


function Grow () {
  return (
    <section id="grow" className="py-11">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center">
            <span>Grow Your Business</span>
            <h2 className="mt-2">Transform Your Entreprise Speedly</h2>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-7">
            <ul>
              <li className="d-flex py-2">
                <div className="px-4">
                  <span className="mx-auto align-items-center justify-content-center number">1</span>
                </div>
                <div className="px-4">
                  <h3>Designer High Level</h3>
                  <p>Officia et fugiat mollit qui. Dolor elit aliqua voluptate ipsum excepteur 
                    cillum consequat consectetur duis magna.
                  </p>
                </div>
              </li>

              <li className="d-flex py-2">
                <div className="px-4">
                  <span className="mx-auto align-items-center justify-content-center number">2</span>
                </div>
                <div className="px-4">
                  <h3>Designer High Level</h3>
                  <p>Officia et fugiat mollit qui. Dolor elit aliqua voluptate ipsum excepteur 
                    cillum consequat consectetur duis magna.
                  </p>
                </div>
              </li>

              <li className="d-flex py-2">
                <div className="px-4">
                  <span className="mx-auto align-items-center justify-content-center number">3</span>
                </div>
                <div className="px-4">
                  <h3>Designer High Level</h3>
                  <p>Officia et fugiat mollit qui. Dolor elit aliqua voluptate ipsum excepteur 
                    cillum consequat consectetur duis magna.
                  </p>
                </div>
              </li>
            </ul>
          </div>
          <div className="col-lg-5">
            <img src={Server} class="img-fluid mt-4" alt="Service-Cloud"/>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Grow;
