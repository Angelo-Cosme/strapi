
import React from "react";


function Slider () {
  return (
    <section id="slider">
      <div className="container">
        <div className="row">
          <div className="col-lg-12 me-auto flex-column d-flex justify-content-center align-items-center">
            <div className="text-header text-center">
              <h1>Your Website Beyond Expectations</h1>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit vel sapien, malesuada congue egestas. Varius condimentum elementum</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Slider;
