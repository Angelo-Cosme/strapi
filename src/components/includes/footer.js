
import React from "react";
import { BsFacebook } from "react-icons/bs";
import { BsLinkedin } from "react-icons/bs";

function Footer () {
  return (
    <div id="footer" className="py-7">
      <div className="container">
        <div className="row wrap-footer">
          <div className="col-lg-12 mx-auto text-center">
            <ul className="socials">
              <li><a href="#" className="link"><BsFacebook /></a></li>
              <li><a href="#" className="link"><BsLinkedin /></a></li>
            </ul>
            <p>© 2022 Strapi Product. All Rights Reserved | Powered by GuruLab</p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Footer;
