
import React from "react";
import AwsSevices from "../images/Amazon_Web_Services.png";
import GoogleCloud from "../images/Google_Cloud.png";
import DigitalOcean from "../images/DigitalOcean.png";
import AzureM from "../images/Microsoft_Azure.png";



function Products () {
  return (
    <div id="product" className="py-11">
      <div className="container">
        <div className="row mx-auto text-center">
          <div className="col-lg-3 col-md-6 col-6">
            <div class="img-logo"><img src={AwsSevices} class="img-fluid" alt="Products"/></div>
          </div>
          <div className="col-lg-3 col-md-6 col-6">
            <div class="img-logo"><img src={GoogleCloud} class="img-fluid" alt="Products"/></div>
          </div>
          <div className="col-lg-3 col-md-6 col-6">
            <div class="img-logo"><img src={DigitalOcean} class="img-fluid" alt="Products"/></div>
          </div>
          <div className="col-lg-3 col-md-6 col-6">
            <div class="img-logo"><img src={AzureM} class="img-fluid" alt="Products"/></div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Products;
