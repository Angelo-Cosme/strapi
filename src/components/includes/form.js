import React from "react";
import ImgContact from '../images/contact.png';




function ContactBanner () {
    return( 
        <section id="contact" class="contact py-11" >
        <div class="container">
          <div class="row content">
            <div class="col-lg-5 col-md-8 col-sm-12">
              <h1>Contact us. It's fast!</h1>
                <p className="mb-4">
                  Need help or more information? A customer service available 24 hours a day
                  to assist you.
                </p>
                <div class="form-contact">
                <form action="https://formspree.io/f/xeqngeke" method="POST">
                  <div class="mb-4"><input type="text" name="name" class="form-control" placeholder="First &  Second Name" /></div>
                  <div class="mb-4"><input type="email" name="email" class="form-control" placeholder="Address E-mail" /></div>
                  <div class="mb-4"><input type="text" name="subjet" class="form-control" placeholder="Subject" /></div>
                  <div class="mb-4"><textarea type="text" name="message" class="form-control" rows="3" placeholder="Your Message"></textarea></div>
                  <button to="#" type="submit" class="btn form-btn float-end">Send</button>
                </form>
              </div>
            </div>
            <div class="offset-1 col-lg-6">
              <div class="img-header">
              <img src={ImgContact} class="img-fluid" alt="Contact-kryptonite" />
              </div>
            </div>
          </div>
       </div>
      </section>
    );
}

export default ContactBanner;