import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
const Navbar = () => {
  const [show, setShow] = useState(false)
  const controlNavbar = () => {
    if (window.scrollY > 100) {
      setShow(true)
    }else {
      setShow(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', controlNavbar)
    return () => {
      window.removeEventListener('scroll', controlNavbar)
    };
  }, [])

  return (
    <div id="menu" className={`sticker-wrapper ${show && 'nav-wrapper'}`}>
      <nav class="navbar navbar-expand-lg navbar-light">
        <div class="container">
          <Link class="navbar-brand" to="/">Strapi Product</Link>
          <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
            <div class="navbar-nav">
              <Link class="nav-link btn-info" to="/Contact">Get Start</Link>
            </div>
          </div>
        </div> 
      </nav>
    </div>
  );
}
export default Navbar;
