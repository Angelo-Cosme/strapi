import './App.css';
import '../src/theme.css'; 

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';

import Navbar from './components/includes/navbar';
import Footer from './components/includes/footer';

import Home from './components/pages/Home';
import Contact from './components/pages/Contact';

import "@fontsource/raleway";

function App() {
  return (
    <>
    <Router>
    <Navbar/>
      <Routes>
        <Route exact path="/" element={<Home />} />
        <Route path="/Contact" element={<Contact />} />
      </Routes>  
    <Footer/>
    </Router>
    </>
  );
}

export default App;
